const http = require("http"); 

const express = require("express");


const bodyparser = require("body-parser");

const app = express();

app.set("view engine","ejs");

app.use(express.static( __dirname +"/public"));
app.use(bodyparser.urlencoded({extended:true}));

//boleto viaje
app.get("/boletoviaje", (req, res)=> {
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazo: req.query.plazo
    
    }
    res.render("boletoviaje", params);
});

app.post("/boletoviaje", (req, res)=> {
    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazo: req.body.plazo
    }
    res.render("boletoviaje", params);
});















/* La página del error va al final de get/post */
app.use((req, res, next)=> {
    res.status(404).sendFile(__dirname + '/public/error.html');
});

const puerto = 500;

app.listen(puerto, ()=> {
    console.log("Iniciando puerto");
});